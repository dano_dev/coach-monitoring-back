FROM node:12-alpine as dev

LABEL maintainer="sohyona <sohyona@dano.me>"

RUN apk add tzdata && \
    cp /usr/share/zoneinfo/Asia/Seoul /etc/localtime && \
    echo "Asia/Seoul" > /etc/timezone && \
    apk del tzdata

ENV NODE_ENV=development
WORKDIR /app

COPY package.json .
COPY yarn.lock .

RUN npm set registry https://npm.dano.xyz && \
    yarn install

COPY . .
RUN yarn build

CMD [ "yarn", "start:dev" ]

FROM node:12-alpine as prod

LABEL maintainer="sohyona <sohyona@dano.me>"

RUN apk add tzdata && \
    cp /usr/share/zoneinfo/Asia/Seoul /etc/localtime && \
    echo "Asia/Seoul" > /etc/timezone && \
    apk del tzdata

ARG NODE_ENV=production
ENV NODE_ENV=${NODE_ENV}
WORKDIR /app

COPY --from=dev /app/package.json package.json
COPY --from=dev /app/yarn.lock yarn.lock

RUN npm set registry https://npm.dano.xyz && \
    yarn install --production=true

COPY --from=dev /app/dist dist
COPY --from=dev /app/yarn.lock yarn.lock
COPY --from=dev /app/.env .env

CMD [ "yarn", "start:prod" ]

