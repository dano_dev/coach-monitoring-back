import { Schema } from 'mongoose';
import { Schedule } from './index';

export const CoachingTimeSchema: Schema = new Schema({
  schedule: { type: Schema.Types.ObjectId, ref: 'Schedule' },
  date: { type: Date },
  time: { type: String },
  groupName: { type: String },
  groupId: { type: Number },
  status: { type: Number },
});

export interface CoachingTime {
  _id: string;
  schedule: Schedule;
  date: Date;
  time: string;
  groupName: string;
  groupId: number;
  status: number;
}
