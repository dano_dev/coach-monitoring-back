import { Schema } from 'mongoose';

export const CoachSchema: Schema = new Schema({
  userId: { type: Number },
  name: { type: String },
  myid: { type: String },
});
CoachSchema.index({ userId: 1 });

export interface Coach {
  _id: string;
  userId: number;
  name: string;
  myid: string;
}
