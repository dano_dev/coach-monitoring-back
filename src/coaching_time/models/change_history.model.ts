import { Schema } from 'mongoose';
import { Coach } from './index';

export const ChangeHistorySchema: Schema = new Schema({
  groupName: { type: String },
  gorupId: { type: Number },
  validFrom: { type: Date },
  coach: { type: Schema.Types.ObjectId, ref: 'Coach' },
  programTypeId: { type: Number },
  affectedCnt: { type: Number },
});

export interface ChangeHistory {
  _id: string;
  groupName: string;
  gorupId: number;
  validFrom: Date;
  coach: Coach;
  programTypeId: number;
  affectedCnt: number;
}
