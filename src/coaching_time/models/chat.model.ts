import { Schema } from 'mongoose';
import { Schedule } from './index';

export const ChatSchema: Schema = new Schema({
  message: { type: String },
  isCoach: { type: Boolean },
  commentId: { type: Number },
  regTimestamp: { type: Date },
  schedule: {
    type: Schema.Types.ObjectId,
    ref: 'Schedule',
  },
});
ChatSchema.index({ schedule: -1, commentId: 1 });

export interface Chat {
  _id: string;
  schedule: Schedule;
  isCoach: boolean;
  message: string;
  regTimestamp: Date;
  commentId: number;
}
