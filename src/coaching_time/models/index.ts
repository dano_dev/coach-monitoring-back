export * from './change_history.model';
export * from './chat.model';
export * from './coach.model';
export * from './coaching_time.model';
export * from './dailylog.model';
export * from './schedule.model';
export * from './user.model';

export interface ValiDatedData {
  tid: number;
  count: Array<Array<number>>;
  groupName: string;
  groupId: number;
  total: number;
  flattedCount: number;
  coachingTime: Array<Array<moment.Moment>>;
  isFulfilled: boolean;
}

export interface CheckByCoachDataByGroupId {
  coachingTime: Array<Array<moment.Moment>>;
  groupName: string;
  count: number;
  total: number;
  isFulfilled: boolean;
  tidList: Array<string>;
}

export interface CheckByCoach {
  data: {
    [key: string]: CheckByCoachDataByGroupId;
  };
  coachInfo: {
    _id: string;
    userId: number;
    name: string;
    myid: string;
  };
}
