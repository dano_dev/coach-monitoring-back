import { Schema } from 'mongoose';

export const UserSchema: Schema = new Schema({
  userId: { type: Number },
  name: { type: String },
  myid: { type: String },
});

export interface User {
  _id: string;
  userId: number;
  name: string;
  myid: string;
}
