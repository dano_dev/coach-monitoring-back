import { Schema } from 'mongoose';
import { Coach, User } from './index';

export const ScheduleSchema: Schema = new Schema({
  tid: { type: Number },
  coach: { type: Schema.Types.ObjectId, ref: 'Coach' },
  user: { type: Schema.Types.ObjectId, ref: 'User' },
  fromDate: { type: Date },
  toDate: { type: Date },
  programTypeId: { type: Number },
});
ScheduleSchema.index({ coach: 1 });

export interface Schedule {
  _id: string;
  tid: number;
  coach: Coach['_id'];
  user: User['_id'];
  fromDate: Date;
  toDate: Date;
  programTypeId: number;
}
