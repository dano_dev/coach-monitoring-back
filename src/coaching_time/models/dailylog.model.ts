import { Schema } from 'mongoose';
import { Coach, Schedule } from './index';

export const DailyLogSchema: Schema = new Schema(
  {
    coach: { type: Schema.Types.ObjectId, ref: 'Coach' },
    date: { type: Date },
    groupId: { type: Number },
    groupName: { type: String },
    coachingTime: { type: String },
    affectedCnt: { type: Number },
    programTypeId: { type: Number },
    affectedScheduleList: [{ type: Schema.Types.ObjectId, ref: 'Schedule' }],
  },
  {
    versionKey: false,
  }
);
DailyLogSchema.index({ groupId: 1 });

export interface DailyLog {
  _id: string;
  coach: Coach;
  date: Date;
  groupId: number;
  groupName: string;
  coachingTime: string;
  affectedCnt: number;
  programTypeId: number;
  affectedScheduleList: Schedule[];
}
