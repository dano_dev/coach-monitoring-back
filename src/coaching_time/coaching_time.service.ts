import { CACHE_MANAGER, Inject, Injectable } from '@nestjs/common';
import { InjectModel } from '@nestjs/mongoose';
import { Cache } from 'cache-manager';
import * as moment from 'moment-timezone';
import { Model } from 'mongoose';
import { convertToComparableDate, getYesterdayDate, timeChunk } from '../misc';
import {
  ChangeHistory,
  Chat,
  CheckByCoach,
  CheckByCoachDataByGroupId,
  Coach,
  CoachingTime,
  DailyLog,
  Schedule,
  User,
  ValiDatedData,
} from './models';

@Injectable()
export class CoachingTimeService {
  constructor(
    @Inject(CACHE_MANAGER) private cacheManager: Cache,
    @InjectModel('CoachingTime')
    private readonly coachingTimeModel: Model<CoachingTime>,
    @InjectModel('Schedule')
    private readonly scheduleModel: Model<Schedule>,
    @InjectModel('Chat')
    private readonly chatModel: Model<Chat>,
    @InjectModel('Coach')
    private readonly coachModel: Model<Coach>,
    @InjectModel('User')
    private readonly userModel: Model<User>,
    @InjectModel('DailyLog')
    private readonly dailyLogModel: Model<DailyLog>,
    @InjectModel('ChangeHistory')
    private readonly changeHistoryModel: Model<ChangeHistory>
  ) {}

  async getDashboardData(): Promise<object> {
    // const cacheKey = 'dashboard_data_' + yesterdayDate;
    // const cachedData = await this.cacheManager.get(cacheKey);
    // if (cachedData) return cachedData;

    const data = {
      violatedCnt: 0,
      unassignedCnt: 0,
      changeCnt: 0,
    };

    const date = getYesterdayDate();
    const violatedList = await this.getViolatedList(date);
    const unassignedList = await this.getUnassignedList(date);
    const chageHistory = await this.getChangeHistory(date, date);
    data.violatedCnt = violatedList.length;
    data.unassignedCnt = unassignedList.length;
    data.changeCnt = chageHistory.length;

    // await this.cacheManager.set(cacheKey, data, {
    //   ttl: 60 * 60 * 24,
    // });

    return data;
  }

  async getAllCoaches(): Promise<Coach[]> {
    return await this.coachModel.find({});
  }

  async insertDailyLog(
    coach: object,
    groupId: number,
    groupName: string,
    coachingTime: string,
    affectedCnt: number,
    date: string,
    programTypeId: number,
    affectedScheduleList: string[]
  ): Promise<object> {
    const dailylog = await this.dailyLogModel.findOne({
      groupId: groupId,
      date: convertToComparableDate(date),
    });
    if (dailylog) return;
    const newDailyLog = new this.dailyLogModel({
      coach,
      date: convertToComparableDate(date),
      groupId,
      groupName,
      coachingTime,
      affectedCnt,
      programTypeId,
      affectedScheduleList,
    });
    newDailyLog.save();

    // const cacheKey = 'dashboard_data_' + yesterdayDate;
    // if (this.cacheManager.get(cacheKey))
    //   await this.cacheManager.remove(cacheKey);
    return newDailyLog;
  }

  async getAllCoachingTimeInfo(): Promise<CoachingTime[]> {
    return await this.coachingTimeModel.find({});
  }

  async getChatData(commentId: number): Promise<Chat> {
    return await this.chatModel
      .find({ commentId: commentId })
      .populate({ path: 'schedule', populate: { path: 'coach' } });
  }

  async getScheduleByOid(tid: string): Promise<CoachingTime> {
    return await this.scheduleModel
      .findOne({
        _id: tid,
      })
      .populate('user')
      .populate('coach');
  }

  async getCoachingTimeInfo(tid: number, date: string): Promise<CoachingTime> {
    const dateFormatted = convertToComparableDate(date).toDate();
    const schedule = await this.scheduleModel
      .findOne({ tid: tid })
      .populate('coach');
    const coachingTimeData = await this.coachingTimeModel
      .findOne({
        schedule: schedule._id,
        date: dateFormatted,
      })
      .populate('schedule');
    return coachingTimeData;
  }

  async getScheduleInfo(tid: Number): Promise<Schedule> {
    return await this.scheduleModel
      .findOne({ tid: tid })
      .populate('user')
      .populate('coach');
  }

  async getChatOfTheDay(
    tid: number,
    date: string,
    coachOnly?: boolean
  ): Promise<Chat[]> {
    const schedule = await this.getScheduleInfo(tid);
    const start = convertToComparableDate(date).toDate();
    const end = convertToComparableDate(date).endOf('day').toDate();

    const chatList = await this.chatModel.find({
      schedule: schedule._id,
      regTimestamp: { $gte: start, $lt: end },
    });

    if (!coachOnly) return chatList;
    return chatList.filter(chat => chat.isCoach);
  }

  // 수강생 개별로 그날의 스케쥴에 맞게 코칭을 받았는지 확인
  async getFeedbackInTimeByTid(
    tid: number,
    date: string
  ): Promise<ValiDatedData> {
    const coachingTime = await this.getCoachingTimeInfo(tid, date); // 해당일의 코칭타임을 불러온다
    const schedule = await this.getScheduleInfo(tid);
    if (!coachingTime || coachingTime.status != 1) return;

    const coachingTimeArr = timeChunk(coachingTime.time, date);
    const temp = {
      tid: tid,
      count: [],
      total: coachingTimeArr.length * 2,
      flattedCount: 0,
      coachingTime: coachingTimeArr,
      isFulfilled: false,
      groupId: coachingTime.groupId,
      groupName: coachingTime.groupName,
      programTypeId: schedule.programTypeId,
    };

    // 채팅을 정해진 코칭타임 내에 했는지 확인하는 작업
    for (let i = 0; i < coachingTimeArr.length; i++) {
      const timeArr = coachingTimeArr[i];
      let inCount = 0;
      let outCount = 0;

      // 토탈과 베이직의 피드백 시간이 다르다.
      // 만약 코칭타임이 10-14시라고 가정했을 때
      // 토탈은 9:30-10:00 사이에 한번, 14:00-14:30 사이에 한번씩 반드시 대화가 있어야하고
      // 베이직은 10시부터 14시 사이 아무때나 대화가 있으면 된다
      if (schedule.programTypeId === 0) {
        const startFrom = timeArr[0].clone().subtract(30, 'minutes');
        const startTo = timeArr[0].clone().add(1, 'minutes');
        const endFrom = timeArr[1].clone().subtract(1, 'minutes');
        const endTo = timeArr[1].clone().add(30, 'minutes');

        const appearChat = await this.chatModel.findOne({
          schedule: schedule,
          isCoach: true,
          regTimestamp: {
            $gte: startFrom,
            $lte: startTo,
          },
        });
        if (appearChat) inCount = 1;

        const exitChat = await this.chatModel.findOne({
          schedule: schedule,
          isCoach: true,
          regTimestamp: {
            $gte: endFrom,
            $lte: endTo,
          },
        });
        if (exitChat) outCount = 1;
      } else {
        const start = timeArr[0];
        const end = timeArr[1];

        const chatInTime = await this.chatModel.findOne({
          schedule: schedule,
          isCoach: true,
          regTimestamp: {
            $gte: start,
            $lte: end,
          },
        });
        if (chatInTime) {
          inCount = 1;
          outCount = 1;
        }
      }

      temp.count.push([inCount, outCount]); // 등장인사, 퇴장인사 여부를 등록한다
    }

    // 한 그룹에서 코칭타임이 2번으로 나누어져 있을 때(예: 10:00-12:00, 15:00-17:00)
    // 코치는 9:30-10:00, 14:30-15:00 에 등장인사를,
    // 12:00-12:30, 17:00-17:30 에 퇴장인사를 반드시 해야한다. 총 4번의 등장/퇴장 메세지를 카운트 해야한다.
    temp.flattedCount = temp.count.reduce((acc, curr) => {
      return acc + curr[0] + curr[1];
    }, 0);

    // 만약 total과 flattedCount의 값이 동일하지 않다면 코칭타임을 지키지 않은 것이다.
    temp.isFulfilled = temp.total === temp.flattedCount;
    return temp;
  }

  async deleteDailyLog(date: string): Promise<object> {
    const dateFormatted = convertToComparableDate(date);
    await this.dailyLogModel.deleteMany({ date: dateFormatted });
    return { error: 0 };
  }

  // 코치가 수강생들에게 제때 코칭타임을 했는지 확인한다
  async checkByCoach(userId: number, date: string): Promise<CheckByCoach> {
    const response = { data: {}, coachInfo: {} } as CheckByCoach;

    const dateFormatted = convertToComparableDate(date).toDate();
    // userId로 코치 정보를 가져온다
    const coach = await this.coachModel.findOne({ userId: userId });
    response.coachInfo = coach;

    // 이 코치와 연결되어 있는 이번달 클래스 수강생 리스트를 전부 가져온다
    const scheduleList = await this.scheduleModel.find({
      coach: coach._id,
      fromDate: { $lte: dateFormatted },
      toDate: { $gte: dateFormatted },
    });

    console.log(moment());
    console.log(scheduleList.length);

    // 수강생이 코칭타임에 피드백을 받았는지 확인
    // 수강생별로 코칭을 받았는지 boolean으로 표시한다
    const validatedDataList = [];
    for (let i = 0; i < scheduleList.length; i++) {
      const validatedByTidData = await this.getFeedbackInTimeByTid(
        scheduleList[i].tid,
        date
      );

      // 비코칭데이거나 코칭타임이 없으면 넣지 않는다
      if (validatedByTidData) validatedDataList.push(validatedByTidData);
    }

    // 수강생별로 코칭 여부 데이터를 사용해 코칭타임 그룹으로 묶어서
    // 코칭타임 그룹내 모든 수강생이 제때 수강을 받았는지 데이터를 정리해준다
    const coachingTimeReport = {} as Record<string, CheckByCoachDataByGroupId>;
    for (let key in validatedDataList) {
      const validatedData = validatedDataList[key];
      if (!(validatedData.groupId in coachingTimeReport))
        coachingTimeReport[validatedData.groupId] = {
          coachingTime: validatedData.coachingTime,
          groupName: validatedData.groupName,
          count: 0,
          total: 0,
          isFulfilled: false,
          tidList: [],
        };
      coachingTimeReport[validatedData.groupId].total += 1;

      if (validatedData.isFulfilled) {
        coachingTimeReport[validatedData.groupId].count += 1;
      } else {
        const userInfo = await this.scheduleModel
          .findOne({ tid: validatedData.tid })
          .populate('user');
        coachingTimeReport[validatedData.groupId].tidList.push(
          userInfo.user._id
        );
      }

      coachingTimeReport[validatedData.groupId].isFulfilled =
        coachingTimeReport[validatedData.groupId].total ===
        coachingTimeReport[validatedData.groupId].count;
    }

    // 코칭타임이 맞지 않는다면 dailylog에 기록한다
    for (let key in coachingTimeReport) {
      const reportData = coachingTimeReport[key];
      if (reportData.isFulfilled) continue; // 코칭타임을 지키지 않은 것만 대해서
      const coachingTime = await this.coachingTimeModel
        .findOne({
          groupId: key,
          date: convertToComparableDate(date),
        })
        .populate('schedule');
      await this.insertDailyLog(
        coach,
        coachingTime.groupId,
        coachingTime.groupName,
        coachingTime.time,
        reportData.total - reportData.count,
        date,
        coachingTime.schedule.programTypeId,
        reportData.tidList
      );
    }

    response.data = coachingTimeReport;

    return response;
  }

  // 하루치 모든 코치의 코칭타임, 채팅을 검사한다
  async updateValidationDaily(date: string): Promise<any> {
    const cacheKey = 'dailyLogUpdating_' + date;
    await this.cacheManager.set(cacheKey, true, {
      ttl: 60 * 60 * 24,
    });

    console.log(`start updateValidationDaily ${cacheKey}`);

    const coachList = (await this.getAllCoaches()) || [];

    for (let i = 0; i < coachList.length; i++) {
      const coachInfo = coachList[i];
      await this.checkByCoach(coachInfo.userId, date);
    }

    console.log(`finished updateValidationDaily ${cacheKey}`);
    console.log(moment());

    await this.cacheManager.set(cacheKey, false, {
      ttl: 60 * 60 * 24,
    });
    return;
  }

  async getViolatedList(date: string): Promise<object[]> {
    if (!date) throw 'Error: Date param is missing';

    const dateFormatted = convertToComparableDate(date).toDate();
    const dailyLogDataList = await this.dailyLogModel.aggregate([
      {
        $lookup: {
          from: this.coachModel.collection.name,
          localField: 'coach',
          foreignField: '_id',
          as: 'coach_doc',
        },
      },
      { $unwind: '$coach_doc' },
      {
        $match: { date: dateFormatted },
      },
      {
        $group: {
          _id: '$coach_doc',
          violationDetail: {
            $push: {
              groupId: '$groupId',
              groupName: '$groupName',
              date: '$date',
              coachingTime: '$coachingTime',
              affectedCnt: '$affectedCnt',
              programTypeId: '$programTypeId',
            },
          },
          totalAffectedCnt: { $sum: '$affectedCnt' },
        },
      },
      { $sort: { totalAffectedCnt: -1 } },
      {
        $project: {
          _id: 1,
          coach: '$_id',
          violationDetail: 1,
        },
      },
    ]);

    // 7/21 기준 오토/체험 유저는 제외하는 로직이 필요
    // 처음부터 오토/체험 유저는 모니터링 DB로 넘어오지 않도록 처리했지만 아직 적용되지 않았음
    const schedule = await this.scheduleModel.findOne({
      fromDate: { $lte: dateFormatted },
      toDate: { $gte: dateFormatted },
    });
    if (!schedule) throw 'Error: schedule does not exist';

    const { fromDate, toDate } = schedule;

    for (let i = 0; i < dailyLogDataList.length; i++) {
      dailyLogDataList[
        i
      ].violatedCntTotal = await this.dailyLogModel.countDocuments({
        date: { $gte: fromDate, $lte: toDate },
        coach: dailyLogDataList[i].coach._id,
      });
    }

    return dailyLogDataList;
  }

  async getUnassignedList(date): Promise<object[]> {
    // const cacheKey = 'unassignedList_' + yesterdayDate;
    // const cachedData = await this.cacheManager.get(cacheKey);
    // if (cachedData) return cachedData;

    const data = [];
    const dateFormatted = convertToComparableDate(date);

    const coachingTimeData = await this.coachingTimeModel
      .find({
        date: dateFormatted,
        status: 3,
      })
      .populate('schedule');

    const unassignedDataList = {};

    for (let i = 0; i < coachingTimeData.length; i++) {
      const x = coachingTimeData[i];
      const coachObjectId = x.schedule.coach;
      if (!(coachObjectId in unassignedDataList)) {
        const coachInfo = await this.coachModel
          .findOne({ _id: coachObjectId })
          .populate('schedule');
        unassignedDataList[coachObjectId] = {
          userId: coachInfo.userId,
          name: coachInfo.name,
          myid: coachInfo.myid,
          count: 0,
        };
      }
      unassignedDataList[coachObjectId].count++;
    }

    for (let key in unassignedDataList) {
      data.push(unassignedDataList[key]);
    }

    // await this.cacheManager.set(cacheKey, data, {
    //   ttl: 60 * 60 * 24,
    // });

    return data;
  }

  async getChangeHistory(start: string, end: string): Promise<object[]> {
    const startDateFormatted = convertToComparableDate(start).toDate();
    const endDateFormatted = convertToComparableDate(end).toDate();
    const changeHistoryList = await this.changeHistoryModel.aggregate([
      {
        $lookup: {
          from: this.coachModel.collection.name,
          localField: 'coach',
          foreignField: '_id',
          as: 'coach_doc',
        },
      },
      { $unwind: '$coach_doc' },
      {
        $match: {
          validFrom: { $gte: startDateFormatted, $lte: endDateFormatted },
        },
      },
      {
        $group: {
          _id: '$coach_doc',
          changeHistoryDetail: {
            $push: {
              groupId: '$groupId',
              groupName: '$groupName',
              affectedCnt: '$affectedCnt',
              programTypeId: '$programTypeId',
              validFrom: '$validFrom',
            },
          },
          totalAffectedCnt: { $sum: '$affectedCnt' },
        },
      },
      { $sort: { totalAffectedCnt: -1 } },
      {
        $project: {
          _id: 0,
          coach: '$_id',
          changeHistoryDetail: 1,
        },
      },
    ]);

    return changeHistoryList;
  }

  async getTidList(groupId: number, date: string): Promise<object[]> {
    const dateFormatted = convertToComparableDate(date);
    const dailyLogs = await this.dailyLogModel.findOne({
      groupId: groupId,
      date: dateFormatted,
    });
    if (!dailyLogs) return [];

    const data = [];
    const userIdList = dailyLogs.affectedScheduleList;
    for (let i = 0; i < userIdList.length; i++) {
      const userId = userIdList[i];
      const userInfo = await this.userModel.findById(userId);
      data.push(userInfo);
    }

    return data;
  }
}
