import { CacheInterceptor, CacheModule, Module } from '@nestjs/common';
import { APP_INTERCEPTOR } from '@nestjs/core';
import { MongooseModule } from '@nestjs/mongoose';
import { CoachingTimeController } from './coaching_time.controller';
import { CoachingTimeService } from './coaching_time.service';
import {
  ChangeHistorySchema,
  ChatSchema,
  CoachingTimeSchema,
  CoachSchema,
  DailyLogSchema,
  ScheduleSchema,
  UserSchema,
} from './models';

const coachingTimeModelModule = MongooseModule.forFeature([
  { name: 'CoachingTime', schema: CoachingTimeSchema },
  { name: 'Schedule', schema: ScheduleSchema },
  { name: 'Chat', schema: ChatSchema },
  { name: 'User', schema: UserSchema },
  { name: 'Coach', schema: CoachSchema },
  { name: 'DailyLog', schema: DailyLogSchema },
  { name: 'ChangeHistory', schema: ChangeHistorySchema },
]);

@Module({
  imports: [coachingTimeModelModule, CacheModule.register()],
  providers: [
    CoachingTimeService,
    {
      provide: APP_INTERCEPTOR,
      useClass: CacheInterceptor,
    },
  ],
  exports: [CoachingTimeService],
  controllers: [CoachingTimeController],
})
export class CoachingTimeModule {}
