import { CACHE_MANAGER, Controller, Get, Inject, Query } from '@nestjs/common';
import { Cache } from 'cache-manager';
import { CoachingTimeService } from './coaching_time.service';

@Controller('coachingtime')
export class CoachingTimeController {
  constructor(
    @Inject(CACHE_MANAGER) private cacheManager: Cache,
    private readonly coachingTimeService: CoachingTimeService
  ) {}

  @Get('dashboard')
  async getDashboardData() {
    const response = { error: 1, message: null, data: {} };

    try {
      const data = await this.coachingTimeService.getDashboardData();
      console.log('getDashboardData : ', data);
      response.data = data;
      response.error = 0;
    } catch (e) {
      console.log('getDashboardData error : ', e);
      response.error = 1;
      response.message = e;
    }

    return response;
  }

  @Get('get_violated_list')
  async getViolatedList(@Query() params) {
    const date = params.date;

    const response = { error: 1, message: null, data: {} };
    try {
      const data = await this.coachingTimeService.getViolatedList(date);
      response.error = 0;
      response.data = data;
    } catch (e) {
      response.error = 1;
      response.message = e;
    }
    const cacheKey = 'dailyLogUpdating_' + date;
    const cachedData = await this.cacheManager.get(cacheKey);
    if (cachedData) {
      response.error = 2;
      response.message =
        '데이터 작업이 진행중입니다. 현재 보이는 데이터가 최신이 아닐 수 있습니다.';
    }

    return response;
  }

  @Get('get_unassigned_list')
  async getUnassignedList(@Query() params) {
    const date = params.date;
    const response = { error: 1, message: null, data: [] };
    try {
      const data = await this.coachingTimeService.getUnassignedList(date);
      response.error = 0;
      response.data = data;
    } catch (e) {
      response.error = 1;
      response.message = e;
    }
    return response;
  }

  @Get('get_change_history')
  async getChangeHistory(@Query() params) {
    const dateStart = params.start;
    const dateEnd = params.end;

    const response = { error: 1, message: null, data: [] };

    try {
      const data = await this.coachingTimeService.getChangeHistory(
        dateStart,
        dateEnd
      );
      response.data = data;
      response.error = 0;
    } catch (e) {
      response.error = 1;
      response.message = e;
    }
    return response;
  }

  @Get('get_tid_list')
  async getTidList(@Query() params) {
    const groupId = params.group_id;
    const date = params.date;

    const response = { error: 1, data: [] };

    try {
      const data = await this.coachingTimeService.getTidList(groupId, date);
      response.data = data;
      response.error = 0;
    } catch (e) {
      response.error = e;
    }
    return response;
  }

  @Get('check_by_coach')
  async checkByCoach(@Query() params) {
    const coach_id = params.coach_id;
    const date = params.date;
    return await this.coachingTimeService.checkByCoach(coach_id, date);
  }

  @Get('delete_daily_log')
  async deleteDailyLog(@Query() params) {
    const date = params.date;
    return await this.coachingTimeService.deleteDailyLog(date);
  }

  @Get('execute')
  async updateValidationDaily(@Query() params) {
    const date = params.date;
    const result = await this.coachingTimeService.updateValidationDaily(date);

    return result;
  }
}
