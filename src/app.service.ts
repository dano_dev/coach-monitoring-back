import { Injectable, Logger } from '@nestjs/common';
import { Cron } from '@nestjs/schedule';
import { CoachingTimeService } from './coaching_time/coaching_time.service';
import { getYesterdayDate } from './misc';

@Injectable()
export class AppService {
  getHello(): string {
    return 'Hello World!';
  }
}

@Injectable()
export class TasksService {
  private readonly logger = new Logger(TasksService.name);

  constructor(private readonly coachingTimeService: CoachingTimeService) {}

  @Cron('0 0 5 * * *')
  updateData() {
    const date = getYesterdayDate();
    this.coachingTimeService.updateValidationDaily(date);
  }
}
