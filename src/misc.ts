import * as moment from 'moment-timezone';

export const standardTimezone = 'Asia/Seoul';

export const timeChunk = (timeStr: string, dateStr: string) => {
  const date = dateStr.slice(0, 10) + 'T';
  const tempArr = [];
  for (let i = 0; i < timeStr.length; i += 8) {
    tempArr.push(timeStr.slice(i, i + 8));
  }

  const timeArr = [];
  tempArr.map(x => {
    let from = x.slice(0, 4);
    let to = x.slice(4);
    from = date + from.slice(0, 2) + ':' + from.slice(2) + ':00.000+09:00';
    to = date + to.slice(0, 2) + ':' + to.slice(2) + ':00.000+09:00';
    const from_formatted = convertToComparableDate(from);
    const to_formatted = convertToComparableDate(to);

    timeArr.push([from_formatted, to_formatted]);
  });
  return timeArr;
};

export const convertToComparableDate = (date: string): moment.Moment => {
  const dateTime = date.length === 10 ? date + 'T00:00:00.000+09:00' : date;
  const dateTimeValidation = moment(dateTime, moment.ISO_8601, true).isValid();
  if (!dateTimeValidation) throw 'Error: Invalid date';

  const d = new Date(dateTime);
  const dateFormatted = moment(d).tz(standardTimezone);

  const formatted = moment(dateFormatted).format('YYYY-MM-DDTHH:mm:ss.SSSz');
  return moment.tz(formatted, 'YYYY-MM-DDTHH:mm:ss.SSSz', standardTimezone);
};

export const getYesterdayDate = (): string => {
  return moment()
    .tz(standardTimezone)
    .add(-1, 'days')
    .format('YYYY-MM-DD');
};
